const booksByCategory = [
    {
        category: "Riqueza",
        books: [
        {
            title: "Os segredos da mente milionária",
            author: "T. Harv Eker",
        },
        {
            title: "O homem mais rico da Babilônia",
            author: "George S. Clason",
        },
        {
            title: "Pai rico, pai pobre",
            author: "Robert T. Kiyosaki e Sharon L. Lechter",
        },
    ],
    },
    {
        category: "Inteligência Emocional",
        books: [
        {
            title: "Você é Insubstituível",
            author: "Augusto Cury",
        },
        {
            title: "Ansiedade – Como enfrentar o mal do século",
            author: "Augusto Cury",
        },
        {
            title: "Os 7 hábitos das pessoas altamente eficazes",
            author: "Stephen R. Covey"
        }
    ]
    }
];

function contarCat() {
    let categoriesCount = booksByCategory.length;
    let booksCountPerCategory = booksByCategory.map(category => ({
        category: category.category,
        count: category.books.length
    }));
    console.log(categoriesCount, booksCountPerCategory);
    let catcounts = '';
    booksCountPerCategory.forEach(item => {
        catcounts += `Categoria: ${item.category}, Contagem: ${item.count}\n`;
    });
    document.getElementById("catlivros").innerHTML = "numero de categorias: " + categoriesCount + " numero de livros por categoria: " + catcounts;
}

function contarAutores() {
    let authors = new Set();
    booksByCategory.forEach(category => {
        category.books.forEach(book => {
            authors.add(book.author);
        });
    });
    console.log(authors.size)
    document.getElementById("numauth").innerHTML = "quantidade de autores: "+ authors.size;


}

//Implementação ruim, se possivel mudar
function setAugusto(){
    livrosPorAutor("Augusto Cury")
}
function livrosPorAutor(authorName) {
    console.log(authorName)
    if(document.getElementById("customauth").value != ""){
        authorName = document.getElementById("customauth").value
    }
    console.log("Mudado")
    console.log(authorName)
    let books = [];
    booksByCategory.forEach(category => {
        category.books.forEach(book => {
            if (book.author === authorName) {
                console.log("PUSHED")
                books.push(book.title);
            }
        });
    });
    console.log(books)
    document.getElementById("customlivros").innerHTML = books
}

    