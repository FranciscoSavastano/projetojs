//Atividade 1
// Função para multiplicar duas matrizes
function multiplicaMatriz(a, b) {
    let resultado = [];
    for (let i = 0; i < a.length; i++) {
        resultado[i] = [];
        for (let j = 0; j < b[0].length; j++) {
            let soma = 0;
            for (let k = 0; k < a[0].length; k++) {
                soma += a[i][k] * b[k][j];
            }
            resultado[i][j] = soma;
        }
    }
    return resultado;
}

//Iniciada quando botao é apertado
function multiplicar() {
    // Entrada do usuario
    let matriz1 = JSON.parse(document.getElementById('input1').value);
    let matriz2 = JSON.parse(document.getElementById('input2').value);

    //Invoca a função e guarda seu valor
    let resultadomatriz = multiplicaMatriz(matriz1, matriz2);

    // Exibir o resultado
    console.log(resultadomatriz);

    //Exibe resultado no HTML
    document.getElementById('resultadomatriz').innerHTML = JSON.stringify(resultadomatriz);
}
//Atividade 2
function calcularCelsius(){
    let f = JSON.parse(document.getElementById('temperaturaF').value);
    f = parseFloat(f)
    // C/5 = (F - 32)/9 pode ser reescrita como C = (F-32) * 5/9
    let resultadoC = (f - 32) * 5/9

    // Exibir o Resultado 
    console.log(resultadoC);

    //Mostra no HTML

    document.getElementById('resultadoC').innerHTML = JSON.stringify(resultadoC);
}
//Atividade 3

function calcularNota(){
    let notas = document.getElementById('notas').value;
    notas = notas.replace(/,/g, '.');
    let notasArray = notas.split(' ').map(parseFloat);
    var soma = notasArray.reduce(function(a, b){
        return a + b
    })
    
    console.log(soma)
    let media = soma / notasArray.length;

    //Exibir media
    console.log(media);
    //Mostrar no HTML
    document.getElementById('media').innerHTML = JSON.stringify(media);


    if(media >= 6){
        console.log("Aprovado");
        document.getElementById("sitfinal").innerHTML =("Aprovado");
    }
    else {
        console.log("Reprovado");
        document.getElementById("sitfinal").innerHTML =("Reprovado");
    }
}
//Atividade 4
//TODO: ATIVIDADE 4


//Atividade 5
function testNumbers(){
    //Retorna um numero aleatorio de 0 a 500
    let numeros = []
    let resposta = []
    for (let i = 0; i < 5; i++) {
        let randomint = Math.floor(Math.random() * 500)
        console.log("numero a ser testado: " + randomint);
        let answer = ''
        if (randomint % 3 === 0 && randomint % 5 === 0) {
            console.log('fizzbuzz');
            answer = "fizzbuzz"
        } else if (randomint % 3 === 0) {
            console.log('fizz');
            answer =  "fizz"
        } else if (randomint % 5 === 0) {
            console.log('buzz');
            answer = "buzz"
        } else {
            console.log('Nada');
            answer = "nada"
        }
        numeros.push(randomint);
        resposta.push(answer)
    }
    console.log(resposta)
    console.log(numeros)
    document.getElementById('numeros').innerHTML = (numeros)
    document.getElementById('respostas').innerHTML = (resposta)
}
//EXTRA 1

function contaPositivo(){
    let nums = document.getElementById('nums').value;
    nums = nums.replace(/,/g, '.');
    let numsArray = nums.split(' ').map(parseFloat);
    let count = numsArray.filter(value => value > 0).length;
    console.log(count);
    document.getElementById('numPos').innerHTML = count + " numeros positivos"
}

//EXTRA 2
//ASSUME QUE ATIVIDADE 2 E SOBRE MULTIPLICAÇÃO
var sumx = 0
function recurse(){
    function startrecurse(x, n) {
        if (n === 0) {
            return 0;
        }
        // Recursão: x é somado à recursão de x e n-1
        else {
            return x + startrecurse(x, n - 1);
        }
    }
    let x = parseInt(document.getElementById("xinput").value)
    let n = parseInt(document.getElementById("ninput").value)
    
    document.getElementById('xnres').innerHTML = startrecurse(x, n);
}


//EXTRA 3
function orderCrescente(){
    //Retorna um int random no intervalo escolhido
    function intRandom(minimo, maximo) {
        min = Math.ceil(minimo);
        max = Math.floor(maximo);
        return Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;
    }
    

    // Gera 5 valores aleatórios entre -300 e 300
    let valores = Array.from({length: 5}, () => intRandom(-300, 300));

    // Ordena em ordem crescente
    let ordenados = [...valores].sort((a, b) => a - b);

    console.log("Valores em ordem crescente:");
    console.log(ordenados);
    document.getElementById('valsorder').innerHTML = "em ordem crescente: " + ordenados

    console.log("\n");

    console.log("Valores na sequência original:");
    console.log(valores);
    document.getElementById('ninput')
    document.getElementById('valsorigin').innerHTML = "ordem original: " + valores
}
//EXTRA 4
function notaParaCaractere() {
    let nota = document.getElementById('extranota').value;
    let letra = ""
    //Compara e define a variavel
    if (nota >= 90) {
        letra = 'A';
    } 
    else if (nota >= 80) {
        letra = 'B';
    } 
    else if (nota >= 70) {
        letra = 'C';
    } 
    else if (nota >= 60) {
        letra = 'D';
    } 
    else {
        letra = 'F';
    }
    //Mostra no console
    console.log(`A nota ${nota} corresponde a letra ${letra}.`);
    //Mostra no html
    document.getElementById("notaletra").innerHTML = "A nota: " + nota + " corresponde a letra: " + letra
}