const familia ={

    receitas: [5000, 300, 200],
    despesas: [800, 100, 400, 50] 
}

function calcularSaldo() {
    const totalReceitas = familia.receitas.reduce((total, valor) => total + valor, 0);
    const totalDespesas = familia.despesas.reduce((total, valor) => total + valor, 0);

    const saldo = totalReceitas - totalDespesas;

    if (saldo > 0) {
        console.log(`A família está com saldo positivo de R$${saldo.toFixed(2)}`);
        document.getElementById("resultSaldo").innerHTML = "A família está com saldo positivo de R$ "+saldo.toFixed(2)
    } else if (saldo < 0) {
        console.log(`A família está com saldo negativo de R$${Math.abs(saldo).toFixed(2)}`);
        document.getElementById("resultSaldo").innerHTML = "`A família está com saldo negativo de R$ " +Math.abs(saldo).toFixed(2)
    } else {
        console.log("A família está com o saldo zerado");
        document.getElementById("resultSaldo").innerHTML = "Saldo da familia esta zerado"
    }
}
